import java.util.Stack;

public class Postfix
{
    Stack<Double> stack;
    Stack<String> stack2;
    
    public Postfix()
    {
        stack = new Stack<Double>();
        stack2 = new Stack<String>();
    }
    
    private double eval(String expr)
    {
        String[] arr = expr.split(" ");
        
        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i].equals("+"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double c = b + a;
                stack.push(c);
            }
            else if (arr[i].equals("-"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double c = b - a;
                stack.push(c);
            }
            else if (arr[i].equals("*"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double c = b * a;
                stack.push(c);
            }
            else if (arr[i].equals("/"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double c = b / a;
                stack.push(c);
            }
            else if (arr[i].equals("^"))
            {
                double a = stack.pop();
                double b = stack.pop();
                double c = Math.pow(b, a);
                stack.push(c);
            }
            else if (arr[i].equalsIgnoreCase("sin"))
            {
                double a = stack.pop();
                double b = Math.sin(a);
                stack.push(b);
            }
            else if (arr[i].equalsIgnoreCase("cos"))
            {
                double a = stack.pop();
                double b = Math.cos(a);
                stack.push(b);
            }
            else if (arr[i].equalsIgnoreCase("tan"))
            {
                double a = stack.pop();
                double b = Math.tan(a);
                stack.push(b);
            }
            else if (arr[i].equalsIgnoreCase("pi"))
            {
                stack.push(Math.PI);
            }
            else
            {
                stack.push(Double.parseDouble(arr[i]));
            }
        }
        return stack.peek();
    }
    
    private int prec(String a)
    {
        switch (a)
        {
            case "+":
            case "-":
                return 1;
                            
            case "*":
            case "/":
                return 2;
            
            case "sin":
            case "cos":
            case "tan":
                return 3;
                
            case "^":
                return 4;
                
            case "(":
            case ")":
                return -1;
                

                
            default:
                return 0;
        }
    }
        
    public double shunt(String expr)
    {
        String output = "";
        String[] arr = expr.split(" ");
        
        for( int i = 0; i < arr.length; i++)
        {
            if ( prec( arr[i] ) == 0 )
            {
                output += arr[i] + " ";
            }
            else if ( arr[i].equals("(") )
            {
                stack2.push( arr[i] );
            }
            else if ( arr[i].equals(")") )
            {
                while (!stack2.peek().equals("(") )
                {
                    output += stack2.pop() + " ";
                }
                stack2.pop();
            }
            else if ( prec( arr[i] ) > 0 )
            {
                while ( ( stack2.size() > 0 ) && ( ( prec( arr[i] ) == 4 )|| ( prec( arr[i] ) <= prec( stack2.peek() ) ) ) )
                {
                    output += stack2.pop() + " ";
                }
                stack2.push( arr[i] );
            }
        }
        
        while ( stack2.size() > 0 )
        {
            output += stack2.pop() + " ";
        }
        return eval( output );
    }
}