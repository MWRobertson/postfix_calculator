import java.util.Stack;
import acm.program.*;
import acm.gui.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.Color;

public class Calculator extends Program
{
    Stack<Double> stack;
    Stack<String> stack2;
    private JTextField infix, postfix, output;
    private JLabel infixLabel, postfixLabel, outputLabel;
    private JButton goButton, clearButton;
    
    public Calculator()
    {
        stack = new Stack<Double>();
        stack2 = new Stack<String>();
        start();
        setSize(400, 200);
        setBackground(Color.GRAY);
    }
    
    public void init()
    {
        TableLayout table = new TableLayout(4, 2);
        setLayout(table);
        
        infix = new JTextField();
        postfix = new JTextField();
        output = new JTextField();
        
        postfix.setEditable(false);
        output.setEditable(false);
        
        Dimension d = infix.getPreferredSize();
        d.setSize(200, d.getHeight());
        infix.setPreferredSize(d);
        
        infixLabel = new JLabel("<html><b>Infix</b></html>");
        infixLabel.setForeground(Color.WHITE);
        postfixLabel = new JLabel("<html><b>Postfix</b></html>");
        postfixLabel.setForeground(Color.WHITE);
        outputLabel = new JLabel("<html><b>Output</b></html>");
        outputLabel.setForeground(Color.WHITE);
        
        goButton = new JButton("Go!");
        clearButton = new JButton("Clear");
        
        add(infixLabel);
        add(infix);
        add(postfixLabel);
        add(postfix);
        add(outputLabel);
        add(output);
        add(goButton);
        add(clearButton);
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent ae)
    {
        String what = ae.getActionCommand();
        
        if (what.equals("Clear"))
        {
            infix.setText("");
            postfix.setText("");
            output.setText("");
        }
        else if (what.equals("Go!"))
        {
            String infixText = infix.getText();
            String postfixText = shunt(infixText);
            String outputText = eval(postfixText);
            
            postfix.setText(postfixText);
            output.setText("" + outputText);
        }
    }
    
    private String eval(String expr)
    {
        String[] arr = expr.split(" ");
        
        try
        {
            for (int i = 0; i < arr.length; i++)
            {
                if (arr[i].equals("+"))
                {
                    double a = stack.pop();
                    double b = stack.pop();
                    double c = b + a;
                    stack.push(c);
                }
                else if (arr[i].equals("-"))
                {
                    double a = stack.pop();
                    double b = stack.pop();
                    double c = b - a;
                    stack.push(c);
                }
                else if (arr[i].equals("*"))
                {
                    double a = stack.pop();
                    double b = stack.pop();
                    double c = b * a;
                    stack.push(c);
                }
                else if (arr[i].equals("/"))
                {
                    double a = stack.pop();
                    double b = stack.pop();
                    double c = b / a;
                    stack.push(c);
                }
                else if (arr[i].equals("^"))
                {
                    double a = stack.pop();
                    double b = stack.pop();
                    double c = Math.pow(b, a);
                    stack.push(c);
                }
                else if (arr[i].equalsIgnoreCase("sin"))
                {
                    double a = stack.pop();
                    double b = Math.sin(a);
                    stack.push(b);
                }
                else if (arr[i].equalsIgnoreCase("cos"))
                {
                    double a = stack.pop();
                    double b = Math.cos(a);
                    stack.push(b);
                }
                else if (arr[i].equalsIgnoreCase("tan"))
                {
                    double a = stack.pop();
                    double b = Math.tan(a);
                    stack.push(b);
                }
                else if (arr[i].equalsIgnoreCase("ln"))
                {
                    double a = stack.pop();
                    double b = Math.log(a);
                    stack.push(b);
                }
                else if (arr[i].equals("!"))
                {
                    double a = stack.pop();
                    double b = 1;
                    for (int q = (int) a; q > 0; q--)
                    {
                        b *= q;
                    }
                    stack.push(b);
                }
                else if (arr[i].equalsIgnoreCase("abs"))
                {
                    double a = stack.pop();
                    double b = Math.abs(a);
                    stack.push(b);
                }
                else if (arr[i].equalsIgnoreCase("pi"))
                {
                    stack.push(Math.PI);
                }
                else
                {
                    stack.push(Double.parseDouble(arr[i]));
                }
            }
            return "" + stack.peek();
        }
        catch(java.lang.NumberFormatException e)
        {
            String cause = e.getMessage();
            return "An error occured: " + cause;
        }
   }
    
    private int prec(String a)
    {
        switch (a)
        {
            case "+":
            case "-":
                return 1;
                            
            case "*":
            case "/":
                return 2;
            
            case "sin":
            case "cos":
            case "tan":
            case "ln":
                return 3;
                
            case "^":
                return 4;
                
            case "(":
            case ")":
            case "!":
            case "|":
                return -1;
                            
            default:
                return 0;
        }
    }
        
    private String shunt(String expr)
    {
        String output = "";
        String[] arr = expr.split(" ");
        boolean a = false;
        
        try
        {
            for( int i = 0; i < arr.length; i++)
            {
                if ( prec( arr[i] ) == 0 )
                {
                    output += arr[i] + " ";
                }
                else if ( arr[i].equals("(") )
                {
                    stack2.push( arr[i] );
                }
                else if ( arr[i].equals(")") )
                {
                    while (!stack2.peek().equals("(") )
                    {
                        output += stack2.pop() + " ";
                    }
                    stack2.pop();
                }
                else if ( arr[i].equals("!") )
                {
                    output += arr[i] + " ";
                }
                else if ( arr[i].equals("|") && a == false)
                {
                    stack2.push( arr[i] );
                    a = true;
                }
                else if ( arr[i].equals("|") && a == true)
                {
                    while (!stack2.peek().equals("|") )
                    {
                        output += stack2.pop() + " ";
                    }
                    stack2.pop();
                    output += "abs ";
                    a = false;
                }
                else if ( prec( arr[i] ) > 0 )
                {
                    while ( ( stack2.size() > 0 ) && ( ( prec( arr[i] ) == 4 )|| ( prec( arr[i] ) <= prec( stack2.peek() ) ) ) )
                    {
                        output += stack2.pop() + " ";
                    }
                    stack2.push( arr[i] );
                }
            }
            
            while ( stack2.size() > 0 )
            {
                output += stack2.pop() + " ";
            }
            return output;
        }
        catch(java.lang.NumberFormatException e)
        {
            String cause = e.getMessage();
            return "An error occured: " + cause;
        }
   }
}