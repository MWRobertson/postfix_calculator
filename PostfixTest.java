

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class PostfixTest.
 *
 * @author  (Michael Robertson)
 * @version (V1)
 */
public class PostfixTest
{
    @Test
    public void testAdd()
    {
        Postfix a = new Postfix();
        assertEquals( 2, a.shunt( "1 + 1" ), .001 );
    }

    @Test
    public void testSubtract()
    {
        Postfix a = new Postfix();
        assertEquals( 1, a.shunt( "2 - 1" ), .001 );
    }
    
    @Test
    public void testMultiply()
    {
        Postfix a = new Postfix();
        assertEquals( 6, a.shunt( "2 * 3" ), .001 );        
    }
    
    @Test
    public void testDivide()
    {
        Postfix a = new Postfix();
        assertEquals( 9, a.shunt( "18 / 2" ), .001 );        
    }
    
    @Test
    public void testExponent()
    {
        Postfix a = new Postfix();
        assertEquals( 8, a.shunt( "2 ^ 3" ), .001 );        
    }
    
    @Test
    public void testSin()
    {
        Postfix a = new Postfix();
        assertEquals( 0.850903524534, a.shunt( "sin ( 45 )" ), .001 );        
    }
    
    @Test
    public void testCos()
    {
        Postfix a = new Postfix();
        assertEquals( 0.525321988818, a.shunt( "cos ( 45 )" ), .001 );          
    }    
    
    @Test
    public void testTan()
    {
        Postfix a = new Postfix();
        assertEquals( 1.619775190544, a.shunt( "tan ( 45 )" ), .001 );              
    }    
    /*
    @Test
    public void testPi()
    {
        Postfix a = new Postfix();
        assertEquals( Math.PI, a.eval( "pi" ), .001 );         
    }    
    */
    @Test
    public void testRigorous()
    {
        Postfix a = new Postfix();
        assertEquals( 0.601036681672, a.shunt( "tan ( cos ( sin ( ( 6 + 5 - 4 ) / 3 * 2 ) ) )" ), .001 );
    }
    
}
