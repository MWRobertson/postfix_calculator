import java.util.Stack;

public class ShuntingYard
{
    Stack<String> stack;
    
    public ShuntingYard()
    {
        stack = new Stack<String>();
    }
    
    private int prec(String a)
    {
        if ((a == "sin") || (a == "cos") || (a == "tan"))
                return 3;
        else if (a == "*" || a == "/")
                return 2;
        else if (a == "+" || a == "-")
                return 1;
        return 0;
    }
    
    private boolean isDouble( String str )
    {
        try
        {
            Double.parseDouble(str);
            return true;
        }
        catch( NumberFormatException e )
        {
            return false;
        }
    }
    
    public String Shunt(String expr)
    {
        String output = "";
        String[] arr = expr.split(" ");
        
        for( int i = 0; i < arr.length; i++)
        {
            if ( isDouble( arr[i] ) == true )
            {
                output += arr[i] + " ";
            }
            else if ( arr[i] == "(" )
            {
                stack.push( arr[i] );
            }
            else if ( arr[i] == ")" )
            {
                while (stack.peek() != "(" )
                {
                    output += stack.pop();
                }
                stack.pop();
            }
            else
            {
                if (prec( arr[i] ) > prec( stack.peek() ))
                {
                    stack.push(arr[i]);
                }
                else
                output += stack.pop();
                {
                    while (prec( arr[i] ) > prec( stack.peek() ))
                    {
                        output += stack.pop();
                    }
                }
            }
        }
        return output;
    }
}
