

/*
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CalculatorTest.
 *
 * @author  (Michael Robertson)
 * @version (V1)
 
public class CalculatorTest
{
    @Test
    public void testAdd()
    {
        Calculator a = new Calculator();
        assertEquals( 2, a.shunt( "1 + 1" ), .001 );
    }

    @Test
    public void testSubtract()
    {
        Calculator a = new Calculator();
        assertEquals( 1, a.shunt( "2 - 1" ), .001 );
    }
    
    @Test
    public void testMultiply()
    {
        Calculator a = new Calculator();
        assertEquals( 6, a.shunt( "2 * 3" ), .001 );        
    }
    
    @Test
    public void testDivide()
    {
        Calculator a = new Calculator();
        assertEquals( 9, a.shunt( "18 / 2" ), .001 );        
    }
    
    @Test
    public void testExponent()
    {
        Calculator a = new Calculator();
        assertEquals( 8, a.shunt( "2 ^ 3" ), .001 );        
    }
    
    @Test
    public void testSin()
    {
        Calculator a = new Calculator();
        assertEquals( 0.850903524534, a.shunt( "sin ( 45 )" ), .001 );        
    }
    
    @Test
    public void testCos()
    {
        Calculator a = new Calculator();
        assertEquals( 0.525321988818, a.shunt( "cos ( 45 )" ), .001 );          
    }    
    
    @Test
    public void testTan()
    {
        Calculator a = new Calculator();
        assertEquals( 1.619775190544, a.shunt( "tan ( 45 )" ), .001 );              
    }    
    
    @Test
    public void testPi()
    {
        Calculator a = new Calculator();
        assertEquals( Math.PI, a.eval( "pi" ), .001 );         
    }    
    
    @Test
    public void testRigorous()
    {
        Calculator a = new Calculator();
        assertEquals( 0.601036681672, a.shunt( "tan ( cos ( sin ( ( 6 + 5 - 4 ) / 3 * 2 ) ) )" ), .001 );
    }
    
}
*/